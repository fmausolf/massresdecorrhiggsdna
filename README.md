# MassResDecorrHiggsDNA

This code is meant to perform the decorrelation of the relative mass-resolution estimator `sigma_m_over_m` and the invariant mass itself for diphoton or H->diphoton events.

It is taken from https://gist.github.com/threiten/2c4a10df9be5e5c5938717a3d33cf9bd#decorrelation and changed such that it can handle the outputs of HiggsDNA (https://gitlab.cern.ch/HiggsDNA-project/HiggsDNA) in `.parquet` format and add the decorrelated mass-resolution-estimator to these files.  